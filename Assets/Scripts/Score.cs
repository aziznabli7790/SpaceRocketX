using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Score : MonoBehaviour

{
    public int x;
    public bool go = true;
    private int ScoreT;
    public TextMeshProUGUI scoretxt;

    void Update()
    {
        x += 1;
        Invoke(scoretxt.text = (x / 20).ToString("0"), 30000);
        //Save the score as int in ScoreT
        ScoreT = Int16.Parse(scoretxt.text);
        //Save the score as int in PlayerPrefs


        PlayerPrefs.SetInt("Score", ScoreT);
        
       
    }
}