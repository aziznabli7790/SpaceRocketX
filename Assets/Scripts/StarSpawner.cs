using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms.Impl;

public class StarSpawner : MonoBehaviour
{
    public GameObject starPrefab; // the star prefab to spawn
    public float spawnIntervalStars = 3f; // the time interval between star spawns
    public float spawnHeight = 0f; // the height at which the stars will spawn
    public float spawnDistance = 30f; // the distance from the center at which the stars will spawn

    private float spawnTimer; // the timer for spawning stars
    private bool isGameOver; // flag indicating whether the game is over

    // score variables
    public int score = 0;
    public TextMeshProUGUI scoretxt;
    private int scoreIncrease = 50;

    void Start()
    {
        spawnTimer = spawnIntervalStars; // set the spawn timer to the interval to spawn immediately
        isGameOver = false; // set the game over flag to false at start
    }

    void Update()
    {
        if (!isGameOver)
        {
            // update the spawn timer and spawn stars if necessary
            spawnTimer -= Time.deltaTime;
            if (spawnTimer <= 0f)
            {
                SpawnStar();
                spawnTimer = spawnIntervalStars;
            }
        }

        // increase score every frame and update score text
        score += 1;
        scoretxt.text = (score / 20).ToString("0");
    }

    void SpawnStar()
    {
        // choose a random horizontal position to spawn the star
        float xPosition = Random.Range(-spawnDistance, spawnDistance);

        // create the spawn position with the specified height and the randomly chosen horizontal position
        Vector3 position = new Vector3(xPosition, spawnHeight, 0f);

        // instantiate the star at the chosen position
        Instantiate(starPrefab, position, Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            score += scoreIncrease;
            Destroy(gameObject);
            scoretxt.text = (score / 20).ToString("0");
        }
    }

    
}