using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class EndGame : MonoBehaviour
{


    public bool isGameOver = false;
    public void GameOver()
    {
        // set the game over flag to true and load the game over scene
        isGameOver = true;
        SceneManager.LoadScene("OutroScene");
    }
    private void OnCollisionEnter(Collision other)
    {
         if (other.gameObject.CompareTag("Player"))
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
            GameOver();

        }


    }
   
}

