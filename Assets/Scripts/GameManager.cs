using UnityEngine;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{
    public GameObject meteorPrefab; // the meteor prefab to spawn
    public GameObject starPrefab; // the meteor prefab to spawn
    public float spawnIntervalMeteors = 3f; // the time interval between meteor spawns
  
    public int maxLives = 3; // the maximum number of lives the player has
    public float gameOverDelay = 2f; // the delay before the game over screen appears
    public GameObject gameOverScreen; // the game over screen object
    public GameObject playerShip; // the player's spaceship object
    public int score = 0; // the player's score

    public float spawnHeight = 0f; // the height at which the meteors will spawn
    public float spawnDistance = 30f; // the distance from the center at which the meteors will spawn

    private int currentLives; // the current number of lives the player has
    private float spawnTimer; // the timer for spawning meteors
    private bool isGameOver; // flag indicating whether the game is over


    void Start()
    {
        currentLives = maxLives; // set the current number of lives to the maximum
        spawnTimer = spawnIntervalMeteors; // set the spawn timer to the interval to spawn immediately
    }

    void Update()
    {
        if (!isGameOver)
        {
            // update the spawn timer and spawn meteors if necessary
            spawnTimer -= Time.deltaTime;
            if (spawnTimer <= 0f)
            {
                SpawnMeteor();
                spawnTimer = spawnIntervalMeteors;
            }



          

        }

        if (spawnIntervalMeteors > 0.5)
        {
            spawnIntervalMeteors -= 0.000005f;
        }



    }

    void SpawnMeteor()
    {
        // choose a random horizontal position to spawn the meteor
        float xPosition = Random.Range(-spawnDistance, spawnDistance);

        // create the spawn position with the specified height and the randomly chosen horizontal position
        Vector3 position = new Vector3(xPosition, spawnHeight, 0f);

        // instantiate the meteor at the chosen position
        Instantiate(meteorPrefab, position, Quaternion.identity);
    }
  



}