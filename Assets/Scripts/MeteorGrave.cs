using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorGrave : MonoBehaviour
{
    private void OnCollisionEnter(Collision Other)
    {

        if (Other.gameObject.CompareTag("Meteor"))
        {
            Destroy(Other.gameObject);
            
        }
    }
}
