using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class OutroScore : MonoBehaviour
{
    private void Start()
    {
        // Load the scene that contains the GameObject with the script you want to access
        SceneManager.LoadScene("SpaceScene", LoadSceneMode.Additive);

        // Get the GameObject with the script you want to access
        GameObject objectWithScript = GameObject.FindWithTag("GameManager");


        if (objectWithScript == null)
        {
            Debug.Log("Couldn't find the GameObject with the name 'GameManager'");
        }

        // Get the script on the GameObject and access the public variable
        StarSpawner script = objectWithScript.GetComponent<StarSpawner>();

        if (script == null)
        {
            Debug.Log("Couldn't find the StarSpawner component on the GameObject");
        }

        int variableValue = script.score;

        // Output the variable value to the console
        Debug.Log(variableValue);

        // Unload the scene to free up memory
        SceneManager.UnloadSceneAsync("SpaceScene");
    }
}

